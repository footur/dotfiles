# BEGIN P10K INSTANT PROMPT ANSIBLE MANAGED BLOCK
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# END P10K INSTANT PROMPT ANSIBLE MANAGED BLOCK

# Change language
export LANG="en_GB.UTF-8"

# weather
alias weather="curl -s --connect-timeout 3 --tlsv1.3 -H 'Accept-Language: de'  'https://wttr.in/Bremen?format=%l:%20%c%t+UV%3A%20%u%20%20%h\n'"
# colorful cat
alias bat='/usr/bin/pygmentize -O style=monokai -f console256 -g'
export LESS='Ri'
alias glow='glow -p'

alias d='docker'
alias k='kubectl'

alias c='xclip -selection clipboard'
alias v='xclip -selection clipboard -o'

alias s='switch'
alias g='git'

alias urldecode='python3 -c "import sys, urllib.parse as ul; \
    print(ul.unquote_plus(sys.argv[1]))"'

alias urlencode='python3 -c "import sys, urllib.parse as ul; \
    print (ul.quote_plus(sys.argv[1]))"'

# Check if file exists. If it's true, source the file.
checkin_source () { [[ -r "$1" ]] && source "$1"; }

# fzf config
checkin_source /usr/share/doc/fzf/examples/key-bindings.zsh

# zsh syntax highlighting
checkin_source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    # change command color
    export ZSH_HIGHLIGHT_STYLES[arg0]='fg=#00af00,bold'
    export ZSH_HIGHLIGHT_STYLES[comment]='fg=#abb2bf'

# zsh autosuggestions
checkin_source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
    # change autosuggestion color
    export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#767676"

eval "$(thefuck --alias)"

if [ $commands[kubectl] ]; then source <(kubectl completion zsh); fi
if [ $commands[helm] ]; then source <(helm completion zsh); fi
if [ $commands[kind] ]; then source <(/usr/bin/kind completion zsh); fi
if [ $commands[pack] ]; then source $(pack completion --shell zsh); fi
if [ $commands[switcher] ]; then checkin_source /usr/local/bin/switch.sh; fi
if [ $commands[glab] ]; then source <(glab completion -s zsh); fi

export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
if [ $commands[go] ]; then export PATH=$PATH:$(go env GOPATH)/bin; fi

# start tmux automatically
if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
    exec tmux new-session -A -s main
fi

# update k8s context in tmux status bar
autoload -Uz add-zsh-hook

function tmux_hook() {
  if [[ -z ${TMUX} ]]; then
    return
  fi
  tmux set-option -gq "@tmux_kubecontext_kubeconfig_#{pane_id}-#{session_id}-#{window_id}" "${KUBECONFIG}"
}

add-zsh-hook precmd tmux_hook

# BEGIN P10K CONFIG FILE ANSIBLE MANAGED BLOCK
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
# END P10K CONFIG FILE ANSIBLE MANAGED BLOCK
source ~/powerlevel10k/powerlevel10k.zsh-theme
